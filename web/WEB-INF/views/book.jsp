<%-- 
    Document   : book
    Created on : 14.12.2021, 21:21:39
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
<title>Book</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
<style type="text/css">
   IMG.fig {
    float: left; /* Обтекание картинки по левому краю */
    padding-right: 10px; /* Отступ слева */
    padding-bottom: 10px; /* Отступ снизу */
}
  </style>
</head>
<body>
 <!-- <div id="nav">
    <ul>
   <!--   <li><a href="#">About</a></li>
     <!-- <li><a href="#">Contact</a></li> 
      <li><a href="registration.jsp">Регистрация</a></li>
      <li><a href="index.jsp">Главная</a></li>
    </ul>
  </div> -->
<div id="showcase2">
  <p><img src="img/ldpr.jpg" width="400" height="600" 
  alt="Иллюстрация" class="fig">
 <h1>Владимир Жириновский - "Политическая классика"</h1>     
  <strong> Издательство: Либерально-демократическая партия России (ЛДПР)<br> Год выпуска: 1996 <br> </strong>
  <p> <br> </p>
  <h2> Описание </h2>
  <h3> Я предлагаю здесь свою политическую классику, классику либерально-демократического преобразования России, классику нашего подхода к геополитическому устройству мира. Я верю, что развитие страны и мира пойдет по этому классическому пути. </h3>
  <p> <br> </p>
  <p> <br> </p>
  <h1> <font color="red"> КНИГА В НАЛИЧИИ!  </h1> 
</div>  
 </body>
</div>

<!--
<div id="wrapper">
  <div id="showcase2">
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam posuere risus eget eros gravida egestas. Sed vulputate interdum nisl. Fusce nibh ligula, ultricies a, ultricies sit amet, bibendum vitae, orci.</p>
  </div>
  <div id="top"> </div>
  <div id="footer">
    <p>&copy; Name | Design by <a rel="nofollow" target="_blank" href="http://writeside.net">Payal Dhar</a> | <a rel="nofollow" target="_blank" href="http://validator.w3.org/check?uri=referer">XHTML</a> | <a rel="nofollow" target="_blank" href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> | I support OpenDesigns.org</p>
  </div>
</div>
-->
</body>
