
<!--
Template Name: Arialogic
Author: <a href="http://www.os-templates.com/">OS Templates</a>
Author URI: http://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: http://www.os-templates.com/template-terms
-->
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>

  <div id="showcase">
    <p>Популярные книги в нашей библиотеке</p>
  </div>
    
  <div id="figure">    <!-- Картинки-->
  <i1>
    <img src="img/10.jpg" alt="" weight='180' height='240'/> 
  
  </i1>
  
  <i2>
    <img src="img/gos.jpg" alt="" weight='180' height='240'/>
  </i2> 
      
      <i3><a
   href="book"><img src="img/ldpr.jpg" alt="" weight='180' height='240'/>  
  </a></i3>
   
    <t1>  Агата Кристи - "10 негритят"</t1>
    <t2> Платон - "Государство"</t2>
    <t3> В.В. Жириновский - </t3>
    <t4> "Политическая классика"</t4>
     
  </div>
  <div id="top"> </div>
  <div id="text">
    <h1>"Знание сила!"</h1>
    <p>Федеральный проект «Культурная среда» направлен на повышение качества жизни граждан путем модернизации инфраструктуры культуры и реновации учреждений от национальных, имеющих мировое значение, — до сельских организаций культуры. Успех пилотных проектов позволяет сделать вывод о востребованности современных библиотек, Минкультуры России наработан опыт и компетенции в подобных проектах.</p>
    <p>С 10 июня 2020 года читатели обслуживаются на прием и выдачу книг, по предварительному заказу книг по любым каналам связи библиотеки. Для посетителей обязательны средства индивидуальной защиты. Внимание: обязательно соблюдение социальной дистанции 1,5 метра.</p>
  </div>
  <!--
  <div id="footer">
    <p>&copy; Name | Design by <a rel="nofollow" target="_blank" href="http://writeside.net">Zahar Lepeshkin</a> | <a rel="nofollow" target="_blank" href="http://validator.w3.org/check?uri=referer">XHTML</a> | <a rel="nofollow" target="_blank" href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> | I support OpenDesigns.org</p>
  </div> 
  
</div>
</body>
</html>